ROOT_DIR       := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL          := $(shell which bash)
SITE_FILE       = $(ROOT_DIR)/site.yml
VERSION         = 1.0.0
ARGS            = $(filter-out $@,$(MAKECMDGOALS))
THEME_SLUG     := blank_template
THEME_PATH      = $(ROOT_DIR)/wordpress/wp-content/themes/$(THEME_SLUG)
UNAME := $(shell uname)
ifeq ($(OS),Windows_NT)
    detected_OS := Windows
else
    detected_OS := $(shell sh -c 'uname -s 2>/dev/null || echo not')
endif

.SILENT: ;               # no need for @
.ONESHELL: ;             # recipes execute in same shell
.NOTPARALLEL: ;          # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell
default: help-default;   # default target
Makefile: ;              # skip prerequisite discovery

.title:
	$(info WP DevBox: $(VERSION))
	$(info )

help-default help: .title
	@echo "                          ====================================================================="
	@echo "                          Help & Check Menu"
	@echo "                          ====================================================================="
	@echo "                    help: Show WP DevBox Help Menu: type: make help"
	@echo "                   check: Check required files"
	@echo "                  status: List containers status"
	@echo "                 version: Show versions"
	@echo "                          ====================================================================="
	@echo "                          Main Menu"
	@echo "                          ====================================================================="
	@echo "                      up: Create and start application or boot suspended machine"
	@echo "                   start: Create and start application or boot suspended machine"
	@echo "                  resume: Boot suspended machine"
	@echo "                 restart: Restart machine"
	@echo "                    stop: Stop machine"
	@echo "                   shell: Login to the 'app' container as 'application' user"
	@echo "                   clean: Destroy VM"
	@echo ""


up start: check
	vagrant up

resume: check
	vagrant resume

stop:
	vagrant suspend

status:
	vagrant status

restart:
	vagrant reload

check:
ifeq ($(wildcard $(SITE_FILE)),)
	$(error Failed to locate the $(SITE_FILE) file.)
endif

check_theme:
ifeq ($(wildcard $(THEME_PATH)),)
	$(error Failed to locate the $(THEME_PATH) theme.)
endif

version: .title
	vagrant version

bash: shell

shell:
	vagrant ssh

clean:
	vagrant destroy

theme_init: check_theme
ifeq ($(detected_OS),Windows)
	vagrant ssh -c "cd /var/www/html/wp-content/themes/$(THEME_SLUG) && npm install"
else
	cd $(THEME_PATH) && npm install
endif

theme_watch: check_theme
ifeq ($(detected_OS),Windows)
	vagrant ssh -c "cd /var/www/html/wp-content/themes/$(THEME_SLUG) && gulp"
else
	cd $(THEME_PATH) && gulp
endif

theme_build: check_theme
ifeq ($(detected_OS),Windows)
	vagrant ssh -c "cd /var/www/html/wp-content/themes/$(THEME_SLUG) && gulp build"
else
	cd $(THEME_PATH) && gulp build
endif

info:
	$(info $(UNAME))

%:
	@:
