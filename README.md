## WP Devbox ![Version 1.0.0](https://img.shields.io/badge/version-v1.0.0-green.svg)
This is a Vagrant configuration designed for development of WordPress plugins, themes, or websites.

# Based on [VCCW](http://vccw.cc/) ![Version 3.3.1](https://img.shields.io/badge/version-v3.3.1-green.svg)

**Recommendation for Windows users!!**
**Install Bash (Windows Subsystem for Linux)**
This is only available on Windows 10.
https://msdn.microsoft.com/en-us/commandline/wsl/install_guide

Open Control Panel.
Click on Programs and Features.
Click on Turn Windows features on or off.
Check Windows Subsystem for Linux (beta).
Click OK and reboot.
Open the program "Bash on Ubuntu on Windows" in the Start menu.
Create a Linux username.


## Requires
* Vagrant 1.8.6 or later
* VirtualBox 5.1.6 or later

## 1. Getting Started
1. Install VirtualBox. <https://www.virtualbox.org/>
1. Install Vagrant. <http://www.vagrantup.com/>

## 2. Install the vagrant-hostsupdater plugin. (Optional)
`$ vagrant plugin install vagrant-hostsupdater`
**Important!!**
Windows does not allow to change hosts files. Please add {hostname from site.yml} 192.168.33.10 by yourself!

## 3. Download vagrant box
`$ vagrant box add vccw-team/xenial64`


## 4. Init (only for MacOS/UNIX and Windows Subsystem for Linux)
1. Run `make up`.
1. Run `make theme_init`.
1. Run `make theme_build`.

## 4. Init (for Windows)
1. Run `vagrant up`.
1. Run `vagrant ssh`.
1. Run `cd /var/www/html/wp-content/themes/blank_template && npm install`.
1. Run `gulp build"`.

## 5. Visit WordPress on the Vagrant in your browser
Visit <http://{hostname from site.yml}/> or <http://192.168.33.10/>

### Note
* The `site.yml` has to be in the same directory with Vagrantfile.
* You can put difference to the `site.yml`.
